import numpy as np

import ase.io as io
from ase.build import bulk
from ase.visualize import view

import DefectLib as DL

atoms=bulk("H","sc",a=1.)

NZ=10
NY=2*NZ
atoms=atoms.repeat((1,NY,NZ))
pos0=atoms.get_positions()
#print pos0[1210]
#print np.linalg.norm(pos0[1210]-pos0[1209])
#print np.linalg.norm(pos0[1210]-pos0[1211])
#print np.linalg.norm(pos0[1210]-pos0[1230])
#print np.linalg.norm(pos0[1210]-pos0[1190])


orig1 = np.array([0,  NY/2, NZ/4+.1])
orig2 = np.array([0,  NY/2, 3.*NZ/4-.1])
print atoms.get_cell(),orig1,orig2

a=1; Mu=25; Nu=.5 
b=1
dislocation_type='screw'
atomsDisp=DL.ApplySelfDisplacement(atoms, Nu,b,orig1,orig2, dislocation_type,ImageNum=0)#,ImageNum=0)

pos=atomsDisp.get_positions()
#pos[:,0]=0.
atomsDisp.set_positions(pos)
#print pos[1210]
#print np.linalg.norm(pos[1210]-pos[1209])
#print np.linalg.norm(pos[1210]-pos[1211])
#print np.linalg.norm(pos[1210]-pos[1230])
#print np.linalg.norm(pos[1210]-pos[1190])


view(atoms)
view(atomsDisp)
