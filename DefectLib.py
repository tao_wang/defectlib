import numpy as np
from ase import *
from math import *

from matplotlib.mlab import griddata
import matplotlib as mpl
from matplotlib import pyplot as plt
from matplotlib import colors as colors

import os
from ase.lattice.spacegroup import crystal
from ase.lattice.triclinic import TriclinicFactory

from ase.lattice.cubic import FaceCenteredCubicFactory
class DiamondFactory(FaceCenteredCubicFactory):
    """A factory for creating diamond lattices."""
    xtal_name = 'diamond'
    bravais_basis = [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]]
Diamond = DiamondFactory()

class ElasticConstant(object):
    ''' elastic constant database'''
    def __init__(self, material):
        self.material = material
        if material == 'Si':
            self.c11 = 3.94*1e2; self.c12 = 1.33*1e2; self.c44 = 1.06*1e2 #GPa
        elif material == 'GaN':
            self.c11 = 1.617*1e2; self.c12 = 0.816*1e2; self.c44 = 0.603*1e2 #GPa
        self.getShearModulus()
        self.getLamdaModulus()
        self.getPossionRatio()
    def getShearModulus(self):
        self.__mu = self.c44-(2.0*self.c44-(self.c11-self.c12))/5.0
        return self.__mu
    def getLamdaModulus(self):
        self.__lamda = self.c12-(2.0*self.c44-(self.c11-self.c12))/5.0
        return self.__lamda
    def getPossionRatio(self):
        self.__nu = self.__lamda/(2.0*(self.__lamda+self.__mu))
        return self.__nu

def PrintNN(atoms,orig,cutoff=5.):
    dist=np.linalg.norm(atoms.get_positions()[:,1:]-orig[1:],axis=1)
    II  = np.argsort(dist)
    II2 = II[dist[II]<cutoff]
    print "Origin: %6.2f %6.2f"%(orig[1],orig[2])
    for i, ii in enumerate(II2):
        if i == 16:
            return
        at=atoms[ii]
        print "%03i %03i %2s %7.3f  %6.2f %6.2f %6.2f "%(i+1, ii+1,at.symbol,dist[ii],
                                                    at.position[0],
                                                    at.position[1],
                                                    at.position[2])

# initial perfect structure
import ase.visualize as visual
from ase import Atoms, Atom
import ase.io as io
def InitialSettings(material, a, c, u, Nx,Ny,Nz, dis_type='a', orthogonal = False):
    if material == 'GaN':
        # unit cell for a-type edge dislocation
        if dis_type == 'a':
            class WurtziteFactory(TriclinicFactory):
                """A factory for creating diamond lattices."""
                xtal_name = 'wurtzite'
                bravais_basis = [[0.0, 0.0, 0.0],
                                 [1/2., 1/3., 1/3.],
                                 [u, 0., 0.],
                                 [u+1/2., 1/3., 1/3.]]
                element_basis = (0, 0, 1, 1)
            Wurtzite = WurtziteFactory()
            if not orthogonal:
                atoms = Wurtzite(symbol={'Ga','Ga','N','N'}, 
                         latticeconstant={'a':c, 'b': a, 
                         'c':a,'alpha': 60, 'beta':90, 'gamma':90})
            else:
                atoms = Wurtzite(symbol={'Ga','Ga','N','N'}, 
                         latticeconstant={'a':c, 'b': a,
                         'c':a,'alpha': 60, 'beta':90, 'gamma':90}, 
                         directions=[[1,0,0], [0,1,0], [0,-0.5,1]])
                '''atoms = Atoms(['Ga', 'Ga', 'N','N'],
                           scaled_positions=[(0, 0, 0), (1/2., 1/3., 1/3.),(3/8., 0, 0),(7/8., 1/3., 1/3.)],
                           cell = [(2*sqrt(2/3.)*a, 0, 0), (0, a, 0), (0, a/2, a*sqrt(3)/2)])'''
        # unit cell for c-type edge dislocation
        elif dis_type == 'c':
            class WurtziteFactory(TriclinicFactory):
                """A factory for creating diamond lattices."""
                xtal_name = 'wurtzite'
                bravais_basis = [[0.0, 0.0, 0.0],
                                 [1/3., 1/2., 1/3.],
                                 [0., u, 0.],
                                 [1/3., u+1/2., 1/3.]]
                element_basis = (0, 0, 1, 1)
            Wurtzite = WurtziteFactory()
            # orthogonal directions=[[1,0,-1], [0,1,0], [1,0,1]]
            # non-orthogonal directions=[[1,0,0], [0,1,0], [-1,1,2]], [[1,0,-1], [0,1,0], [1,1,1]]
            atoms = Wurtzite(directions=[[1,0,-1], [0,1,0], [1,1,1]],
                             symbol={'Ga','Ga','N','N'},
                             latticeconstant={'a': a, 'b':c, 
                             'c':a,'alpha': 90, 'beta':60, 'gamma':90})
        else:
            print "WARNING: unknow dislocation type"
            return
        if atoms.get_number_of_atoms() > 4 :
            pos = atoms.get_scaled_positions()
            pos[2:4,:] = atoms.get_scaled_positions()[4:6,:]
            pos[4:6,:] = atoms.get_scaled_positions()[2:4,:]
            sym = atoms.get_chemical_symbols()
            sym[2:4] = atoms.get_chemical_symbols()[4:6]
            sym[4:6] = atoms.get_chemical_symbols()[2:4]
            atoms.set_chemical_symbols(sym)
            atoms.set_scaled_positions(pos)
            atoms.set_scaled_positions(atoms.get_scaled_positions())
        io.write('POSCAR-UC',atoms, format='vasp',direct=True)
        
    elif material == 'Si':
        atoms = Diamond(directions=[[1,-1,0], [1,1,0], [1,1,2]], latticeconstant=a, size=(1,1,1), symbol='Si', pbc=(1,1,1))
    cells = atoms.get_cell()
    scaled_cells = np.eye(3)
    Ncells = Nx * Ny * Nz
    Natoms_uc = atoms.get_positions().shape[0]
    Natoms_sc = Natoms_uc * Ncells
    Pos=np.zeros((Natoms_sc, 3))
    print Nx, Ny, Nz
    symbols = []
    for ielement in range(Natoms_uc):
        for iz in range(Nz):
            for iy in range(Ny):
                for ix in range(Nx):
                    Pos[ix + Nx * (iy + Ny * (iz + ielement * Nz))] = \
                        np.dot(np.array(np.transpose(cells)), [ix, iy, iz]) + atoms.get_positions()[ielement]
                    symbols += [atoms.get_chemical_symbols()[ielement]]
    New_atoms = atoms.repeat((Nx,Ny,Nz))
    New_atoms.set_chemical_symbols(symbols)
    New_atoms.set_positions(Pos)
    New_atoms.set_scaled_positions(New_atoms.get_scaled_positions())
    return New_atoms

# Compute displacement field according to single edge dislocations
def GetDisplacement(atoms_xyz, Nu, b, core_xyz, dislocation_type = 'edge'):
    atoms_xyz[:] -= core_xyz
    core_dis = np.linalg.norm(atoms_xyz,axis=1)
    Disp = np.zeros(atoms_xyz.shape)
    if dislocation_type == 'edge':
        Disp[:,1] =  b / (2.0*np.pi)*((np.arctan2(atoms_xyz[:,2],atoms_xyz[:,1])+np.pi) + \
                 (atoms_xyz[:,1]*atoms_xyz[:,2])/(2*(1-Nu)*(atoms_xyz[:,1]**2+atoms_xyz[:,2]**2)))
        Disp[:,2] = -b / (2.0*np.pi)*((1-2*Nu)/(4*(1-Nu))*np.log(atoms_xyz[:,1]**2+atoms_xyz[:,2]**2) + \
                 (atoms_xyz[:,1]**2-atoms_xyz[:,2]**2)/(4*(1-Nu)*(atoms_xyz[:,1]**2+atoms_xyz[:,2]**2))) #* np.exp(-core_dis[:]/100.)
    else:
        Disp[:,0] =  b / (2.0*np.pi)*(np.arctan2(atoms_xyz[:,2],atoms_xyz[:,1])+np.pi)
    return Disp

def ApplyZeroDisplacementBoundary(atoms, Disp):
    '''
    eliminate the lineAtomsar deformation field to 
    fulfill Zero displacement boundary condition
    '''
    #uc = io.read('uc-c-type', format='vasp')
    natom = 2 #len(uc.get_chemical_symbols())
    ErrorAmp = Disp[len(atoms)/natom-1]-Disp[0]
    ErrorDis = atoms.get_positions()[len(atoms)/natom-1]-atoms.get_positions()[0]+[1e-20, 0., 0.]
    print len(atoms)/natom
    print ErrorDis
    return ErrorAmp/ErrorDis*(atoms.get_positions()-atoms.get_positions()[0]) + Disp[0]
    
def GetZeroBoundaryDisplacementCorrection(atoms, Ny, Nz, Disp, dislocation_type):
    '''
    eliminate the lineAtomsar deformation field to 
    fulfill Zero displacement boundary condition
    '''
    #uc = io.read('uc-c-type', format='vasp')
    nucs = Ny*Nz
    natom_uc = atoms.get_number_of_atoms()/nucs
    Error = np.zeros((natom_uc*nucs, 3))
    for iatom_uc in xrange(natom_uc):
        OA = atoms.get_positions()[Ny-1 + nucs*iatom_uc] - atoms.get_positions()[nucs*iatom_uc]
        OB = atoms.get_positions()[(Nz-1)*Ny + nucs*iatom_uc] - atoms.get_positions()[nucs*iatom_uc]
        if dislocation_type == 'edge':
            for idim in xrange(2):
                OA[0] = Disp[Ny-1 + nucs*iatom_uc, idim]
                OB[0] = Disp[(Nz-1)*Ny + nucs*iatom_uc, idim]
                norm = np.cross(OA, OB)
                norm /= norm[0]
                Error[nucs*iatom_uc:nucs*(iatom_uc+1),idim] = Disp[nucs*iatom_uc, idim]-(np.dot( 
                    (atoms.get_positions()[nucs*iatom_uc:nucs*(iatom_uc+1)] -\
                    atoms.get_positions()[nucs*iatom_uc])[:,1:],norm[1:]))
        else:
            OA[0] = Disp[Ny-1 + nucs*iatom_uc, 0]
            OB[0] = Disp[(Nz-1)*Ny + nucs*iatom_uc, 0]
            norm = np.cross(OA, OB)
            norm /= norm[0]
            Error[nucs*iatom_uc:nucs*(iatom_uc+1),0] = Disp[nucs*iatom_uc, 0]-(np.dot( 
                (atoms.get_positions()[nucs*iatom_uc:nucs*(iatom_uc+1)] -\
                atoms.get_positions()[nucs*iatom_uc])[:,1:],norm[1:]))
        
    #PlotAppliedDisplacementField(atoms.get_positions(), Error)
    return Error
    
# Displacement: U_pos = self_displacement + image_displacement + error_displacement
def ApplySelfDisplacement(atoms, Nu, b, orig1,orig2, dislocation_type, ImageNum = 50):
    Disp = np.zeros(atoms.get_positions().shape)
    for Ry in xrange(-ImageNum,1+ImageNum):
        for Rz in xrange(-ImageNum,1+ImageNum):
            Corig = np.dot([0,Ry,Rz],atoms.get_cell())
            orig11=orig1+Corig
            orig21=orig2+Corig
            # displacement induced by left dislocation       
            Disp += GetDisplacement(atoms.get_positions() , Nu, b, orig11, dislocation_type)
            # displacement induced by right dislocation dipole (negative sign)
            Disp -= GetDisplacement(atoms.get_positions() , Nu, b, orig21, dislocation_type)
    FinalPos = atoms.get_positions() + Disp
    atomsDisp=atoms.copy()
    atomsDisp.set_positions(FinalPos)
    return atomsDisp

# Displacement: U_pos = self_displacement + image_displacement + error_displacement
def ApplySelfDisplacement_cubic(a, atoms, Nu, b, orig1,orig2, ImageNum = 50):
    Disp = np.zeros(atoms.get_positions().shape)
    BoxLy = (3 * ImageNum + 2) * atoms.get_cell()[1,1]
    BoxUpLeftY = (np.dot([0, -ImageNum, ImageNum+1],atoms.get_cell()))[1]
    BoxRightBoundary = BoxLy - (2 * ImageNum + 1) * atoms.get_cell()[2,1] + BoxUpLeftY
    print BoxLy, BoxUpLeftY, BoxRightBoundary
    for Ry in xrange(-ImageNum,2*(1+ImageNum)):
        for Rz in xrange(-ImageNum,1+ImageNum): #xrange(-ImageNum,1+ImageNum):
            Corig = np.dot([0,Ry,Rz],atoms.get_cell())
            Corig[1] = Corig[1] - int((Corig[1]+atoms.get_cell()[1,1]+atoms.get_cell()[2,1]) / BoxRightBoundary)*BoxLy
            orig11=orig1+Corig
            orig21=orig2+Corig
            # displacement induced by left dislocation       
            Disp += GetDisplacement(atoms.get_positions() , Nu, b, orig11)
            # displacement induced by right dislocation dipole (negative sign)
            Disp -= GetDisplacement(atoms.get_positions() , Nu, b, orig21)
    #enforce zero displacement boundary condition
    ZeroBoundaryDisp = ApplyZeroDisplacementBoundary(atoms, Disp)
    #PlotAppliedDisplacementField(atoms.get_positions(), ZeroBoundaryDisp)
    FinalPos = atoms.get_positions() + Disp 
    if ImageNum >= 0:
        FinalPos -= ZeroBoundaryDisp
    atomsDisp=atoms.copy()
    atomsDisp.set_positions(FinalPos)
    return atomsDisp
    
def PlotAppliedDisplacementField(pos, Disp):
    '''
    plot the applied displacement filed to check 
    if the applied displacement field is correct
    '''
    xi=np.linspace(pos[:, 1].min(), pos[:, 1].max(), 300)
    yi=np.linspace(pos[:, 2].min(), pos[:, 2].max(), 300)
    ux = griddata(pos[:, 1], pos[:, 2], Disp[:,1], xi, yi, interp='nn')
    uy = griddata(pos[:, 1], pos[:, 2], Disp[:,2], xi, yi, interp='nn')
    fig, ax = plt.subplots(1, 2, figsize=(8, 4), sharex=True, sharey=True)
    cs0 = ax[0].contourf(xi, yi, ux, 100,label = '$u_y$')
    cs1 = ax[1].contourf(xi, yi, uy, 100,label = '$u_z$')
    ax[0].scatter(pos[:, 1], pos[:, 2], marker='o', c='black', s=10, alpha=0.5, edgecolors='none')
    ax[1].scatter(pos[:, 1], pos[:, 2], marker='o', c='black', s=10, alpha=0.5, edgecolors='none')
    fig.colorbar(cs0, ax=ax[0], shrink=0.9)
    fig.colorbar(cs1, ax=ax[1], shrink=0.9)
    plt.savefig('displace_fit.png', transparent=False, dpi=300)

def ResizeBox(atoms, a, c, u, Nx, Ny, Nz):
    print a, c, u, Nx, Ny, Nz
    atoms.set_pbc([1,1,1])
    atoms.set_scaled_positions(atoms.get_scaled_positions() + [0.1, 0., 0.])
    resized_atoms = atoms.copy()
    a0 = (atoms.get_cell()/[Nx, Ny, Nz])[1,1]
    c0 = (atoms.get_cell()/[Nx, Ny, Nz])[0,0]
    u0 = atoms.get_scaled_positions()[atoms.get_number_of_atoms()/2, 0] - \
         atoms.get_scaled_positions()[0, 0]
    cell = atoms.get_cell() * [c/c0, a/a0, a/a0]
    print cell
    print atoms.get_cell()
    scale_pos = atoms.get_scaled_positions() - [u0 - u + 0.1, 0., 0.]
    scale_pos[:,0] -= scale_pos[0,0]
    (scale_pos[:,0])[np.abs(scale_pos[:,0]) < 1.e-10] = 0.
    pos = np.dot(scale_pos, cell)
    resized_atoms.set_positions(pos)
    resized_atoms.set_cell(cell)
    return resized_atoms
